#!/bin/bash

ARTIFACT=$1
TEST_PATH=$2

docker-compose --compatibility down
docker-compose --compatibility up -d

echo "Waiting Tomcat start up...";
until [ "$(curl --connect-timeout 1 --max-time 1 -s -o /dev/null -w "%{http_code}" http://localhost:8080 | grep '200')" != "" ]; do
  sleep 1
done

echo "Deploying the artifact...";
docker cp "$ARTIFACT" tomcat:/usr/local/tomcat/webapps/"$ARTIFACT"

if [ ! -z "$TEST_PATH" ]; then
  until [ "$(curl --connect-timeout 1 --max-time 1 -s -o /dev/null -w "%{http_code}" http://localhost:8080"$TEST_PATH" | grep '200')" != "" ]; do
    sleep 1
  done
  echo "Deployed Successfully";
fi
