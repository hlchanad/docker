
#JAVA_OPTS="$JAVA_OPTS -Xms300m";
#JAVA_OPTS="$JAVA_OPTS -Xmx500m";

JAVA_OPTS="$JAVA_OPTS -XX:+UseCompressedOops";
#JAVA_OPTS="$JAVA_OPTS -XX:+UseConcMarkSweepGC";
#JAVA_OPTS="$JAVA_OPTS -XX:+UseParNewGC";
JAVA_OPTS="$JAVA_OPTS -XX:MaxMetaspaceSize=512m";

JAVA_OPTS="$JAVA_OPTS -Djava.security.egd=file:/dev/./urandom";
JAVA_OPTS="$JAVA_OPTS -Djava.awt.headless=true";
